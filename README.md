## CRS4-ANN-LIS
This code was developed for the project named IPOACUSIA of CRS4 in collaboration with Sardegna Ricerche and Regione Sardegn for the implementation of a tool for recognizing the Italian Sign Language.

The dataset used is created ad-hoc for the primary purpose of the project, which is used to machine learning and deep learning techniques for the training of the recognition system.

# Install and run
To run the neural network demonstrator follow the steps below:

A ```requirements.txt ``` file for installation is provided. The project is developed in  ``` conda ``` evironment.

After downloaded the source code run as described:
```
git clone https://gitlab.com/amanchinu/crs4-ann-LIS.git
cd crs4-ann-LIS
python main.py
```
